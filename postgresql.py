import psycopg2 as pg
from db_config import config
from datetime import datetime, timedelta
import os
import ast
from reporter import send_report
import pprint
import csv
import json

# read connection parameters
params = config()


def connect():
    """ Connect to the postgresql database server """
    conn = None
    try:

        # connect to the postgresql server
        print("Connecting to the postgresql database..")
        conn = pg.connect(**params)

        # create cursor
        cur = conn.cursor()

        # execute statement
        print("Postgresql Database Version: ")
        cur.execute("SELECT version()")

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        # close the cursor
        cur.close()
        print("Cursor closed..")

    except (Exception, pg.DatabaseError) as error:
        print("Oops: ", error)

    finally:
        # close the connection / conn
        if conn:
            conn.close()
            print("Connection closed..")


def insert_dropped_request(job_id, url):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO dropped_item(job_id, url) VALUES(%s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (job_id, url))

        # commit the changes
        conn.commit()
        print("%s was inserted into dropped item table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def get_dropped_item():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM dropped_item
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            item_id = row[0]
            job_id = row[1]
            url = row[2]
            result.append(dict(
                item_id=item_id,
                job_id=job_id,
                url=url,
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def update_status_cancel_job(job_id):
    sql = """
    UPDATE job SET cancel=TRUE WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (job_id,))

        # commit the changes
        conn.commit()
        print("job %s status was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def update_speed_by_job_id(speed, job_id):
    sql = """
    UPDATE job SET speed=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (speed, job_id,))

        # commit the changes
        conn.commit()
        print("job %s speed was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def create_tables():
    """ create tables """

    # commands = (
    #             """
    #                CREATE TABLE request (
    #                     job_id INTEGER,
    #                     url VARCHAR,
    #                     http_status_code VARCHAR(50),
    #                     content_type VARCHAR(150),
    #                     content_length VARCHAR(50),
    #                     created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                     exception_content VARCHAR(250),
    #                     FOREIGN KEY (job_id) REFERENCES job (job_id)
    #                     ON UPDATE CASCADE ON DELETE CASCADE
    #                 )
    #             """,
    # )

    # commands = """
    #            CREATE TABLE task (
    #                 task_id SERIAL PRIMARY KEY,
    #                 task_name VARCHAR(255) UNIQUE NOT NULL,
    #                 index_url VARCHAR(255),
    #                 product_url VARCHAR(255),
    #                 brand_filter VARCHAR(200),
    #                 detail_filter VARCHAR(255),
    #                 task_type VARCHAR(50),
    #                 status VARCHAR(50) DEFAULT 'incomplete',
    #                 requested_by VARCHAR(100)
    #             )
    #             """

    commands = (
        """
           CREATE TABLE job (
                job_id SERIAL PRIMARY KEY,
                url VARCHAR(200),
                daruma_code VARCHAR(50),
                created_at timestamp without time zone NOT NULL DEFAULT now(),
                created_by VARCHAR(150),
                start_time VARCHAR(50),
                end_time VARCHAR(50),
                error_count INTEGER,
                status VARCHAR(50) default 'pending',
                spider VARCHAR(50),
                priority BOOLEAN DEFAULT FALSE,
                requests NUMERIC default 0,
                requests_200 NUMERIC default 0,
                process_id VARCHAR(50),
                unit_id VARCHAR(50),
                speed NUMERIC default 0,
                repeat_days NUMERIC default 0,
                schedule_time timestamp without time zone
            )
        """,
    )
    #     """
    #        CREATE TABLE request (
    #             job_id INTEGER,
    #             url VARCHAR,
    #             http_status_code VARCHAR(50),
    #             content_type VARCHAR(150),
    #             content_length VARCHAR(50),
    #             created_at timestamp without time zone NOT NULL DEFAULT now(),
    #             exception_content VARCHAR(250),
    #             FOREIGN KEY (job_id) REFERENCES job (job_id)
    #             ON UPDATE CASCADE ON DELETE CASCADE
    #         )
    #     """,
    #     """
    #        CREATE TABLE item (
    #             item_id SERIAL PRIMARY KEY,
    #             price INTEGER,
    #             url VARCHAR(250),
    #             data VARCHAR,
    #             created_at timestamp without time zone NOT NULL DEFAULT now()
    #         )
    #     """
    # )

    # commands = ("""
    #                        CREATE TABLE users (
    #                          user_id SERIAL PRIMARY KEY,
    #                          username VARCHAR(50) UNIQUE,
    #                          password VARCHAR(50)
    #                         )
    #             """,)
    #
    # commands = ("""
    #                    CREATE TABLE job (
    #                         job_id SERIAL PRIMARY KEY,
    #                         url VARCHAR,
    #                         daruma_code VARCHAR(50),
    #                         created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                         created_by VARCHAR(150),
    #                         start_time timestamp without time zone,
    #                         end_time timestamp without time zone,
    #                         error_count NUMERIC DEFAULT 0,
    #                         status VARCHAR DEFAULT 'pending',
    #                         spider VARCHAR,
    #                         priority BOOLEAN DEFAULT FALSE,
    #                         requests NUMERIC DEFAULT 0,
    #                         requests_200 NUMERIC DEFAULT 0,
    #                         process_id VARCHAR(50),
    #                         unit_id VARCHAR(50),
    #                         speed NUMERIC DEFAULT 0
    #                     )
    #                 """,
    #             )
    #                 """
    #                        CREATE TABLE request (
    #                          job_id INTEGER,
    #                          url VARCHAR,
    #                          http_status_code VARCHAR,
    #                          content_type VARCHAR,
    #                          content_length VARCHAR,
    #                          created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                          exception_content VARCHAR,
    #                          FOREIGN KEY (job_id) REFERENCES job (job_id)
    #                          ON UPDATE CASCADE ON DELETE CASCADE
    #                         )
    #                 """,
    #             """
    #                                CREATE TABLE item (
    #                                     item_id SERIAL PRIMARY KEY,
    #                                     job_id INTEGER,
    #                                     url VARCHAR,
    #                                     daruma_code VARCHAR(100),
    #                                     data VARCHAR NOT NULL,
    #                                     created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                                     status VARCHAR DEFAULT 'not inserted',
    #                                     spider VARCHAR
    #                                 )
    #             """,
    #             )

    # commands = ("""
    #                                CREATE TABLE auto_brand_name_table (
    #                                  id SERIAL PRIMARY KEY,
    #                                  regex VARCHAR,
    #                                  brand_name VARCHAR,
    #                                  status VARCHAR DEFAULT 'pending'
    #                                 )
    #
    #                     """,
    #                     )

    # commands = (
    #     """
    #                                        CREATE TABLE item (
    #                                             item_id SERIAL PRIMARY KEY,
    #                                             job_id INTEGER,
    #                                             url VARCHAR,
    #                                             daruma_code VARCHAR(100),
    #                                             data VARCHAR NOT NULL,
    #                                             created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                                             status VARCHAR DEFAULT 'not inserted',
    #                                             spider VARCHAR
    #                                         )
    #                     """,
    # )

    # commands = (
    #     """
    #                        CREATE TABLE dropped_item (
    #                             item_id SERIAL PRIMARY KEY,
    #                             job_id INTEGER,
    #                             url VARCHAR,
    #                             created_at timestamp without time zone NOT NULL DEFAULT now()
    #                         )
    #     """,
    # )

    conn = None

    try:
        # connect to the database
        conn = pg.connect(**params)
        # create cursor
        cur = conn.cursor()

        # create table one by one
        print("Creating table one by one, please wait..")
        for command in commands:
            cur.execute(command)
        print("All tables has been created..")
        # close cursor
        cur.close()

        # commit the changes
        conn.commit()
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the connection
        if conn is not None:
            conn.close()


def insert_task(task_name, index_url=None, product_url=None, brand_filter=None, detail_filter=None, task_type=None,
                requested_by=str()):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO task(task_name, 
    index_url, 
    product_url, 
    brand_filter, 
    detail_filter, 
    task_type, 
    requested_by) 
    VALUES(%s, %s, %s, %s, %s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (task_name, index_url, product_url, brand_filter, detail_filter, task_type, requested_by,))

        # commit the changes
        conn.commit()
        print("%s was inserted into task table.." % (task_name))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def insert_job(url, created_by, daruma_code, spider, priority, schedule_time, repeat_days=0):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO job(url, 
    created_by, daruma_code, spider, priority, schedule_time, repeat_days) 
    VALUES(%s, %s, %s, %s, %s, %s, %s)
    """

    # url = url[:int(str(url).find('?'))] if '?' in url else url

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (url, created_by, daruma_code, spider, priority, schedule_time, repeat_days))

        # commit the changes
        conn.commit()
        print("%s was inserted into job table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_failed_jobs():

    conn = None
    result = list()
    query = """
    SELECT * FROM job WHERE requests_200=1 AND status='done' ORDER BY job_id DESC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7] if row[7] is not None else 0
            status = row[8]
            spider = row[9] if row[9] else "-"
            url_display = '%s..' % row[1][:50] if len(url) > 50 else url
            requests = row[11]
            requests_200 = row[12]
            priority = row[10]
            unit_id = row[14] if row[14] is not None else '-'
            speed = row[15] if row[15] is not None else 0
            repeat_days = row[16] if row[17] is not None else '-'
            schedule_time = row[17] if row[16] is not None else '-'
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               url_display=url_display,
                               requests=requests,
                               requests_200=requests_200,
                               spider=spider,
                               priority=priority,
                               unit_id=unit_id,
                               speed=speed,
                               schedule_time=schedule_time,
                               repeat_days=repeat_days))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def delete_requests(id):
    conn = None
    query = """
    DELETE FROM request WHERE job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (id,))

        conn.commit()
        print("requests job id %s was deleted" % id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()


def update_status_job_and_requests(job_id, status):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE job SET status=%s, requests_200=%s, requests=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (status, 0, 0, job_id,))

        # commit the changes
        conn.commit()
        print("job %s status was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def auto_insert_job(url, created_by, daruma_code, spider, priority, schedule_time, repeat_days=0):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO job(url, 
    created_by, daruma_code, spider, priority, schedule_time, repeat_days) 
    VALUES(%s, %s, %s, %s, %s, %s, %s)
    """

    # url = url[:int(str(url).find('?'))] if '?' in url else url

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (url, created_by, daruma_code, spider, priority, schedule_time, repeat_days))

        # commit the changes
        conn.commit()
        print("%s was inserted into job table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def insert_auto_brand_name(values):
    """ insert vendor into vendors table """

    base_sql = """
    INSERT INTO auto_brand_name_table(regex, 
    brand_name) 
    VALUES 
    """

    # url = url[:int(str(url).find('?'))] if '?' in url else url

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        values_sql = []
        values_data = []

        for value_list in values:
            placeholders = ['%s' for i in range(len(value_list))]
            values_sql.append("(%s)" % ', '.join(placeholders))
            values_data.extend(value_list)

        sql = "%s%s" % (base_sql, ', '.join(values_sql))

        cur.execute(sql, values_data)

        # commit the changes
        conn.commit()
        print("all auto-brand-name data was inserted..")
        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def insert_rating(item_url, rating):
    """ insert rating """

    sql = """
    UPDATE price SET rating=%s WHERE item_url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (rating, item_url))

        # commit the changes
        conn.commit()
        print("%s rating was updated into price table.." % (item_url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def insert_request(job_id, url, http_status_code, content_type):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (job_id, url, http_status_code, content_type))

        # commit the changes
        conn.commit()
        print("%s was inserted into request table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def insert_item(url, job_id, data, tokped_product_id, daruma_code=''):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO item(url, job_id, data, tokped_product_id, daruma_code) VALUES(%s, %s, %s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (url, job_id, data, tokped_product_id, daruma_code,))

        # commit the changes
        conn.commit()
        print("%s was inserted into item table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def update_item(url, job_id, data, daruma_code):
    sql = """
    UPDATE item SET  
    job_id=%s, 
    data=%s,
    daruma_code=%s
    WHERE url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        last_update = datetime.datetime.now()
        cur.execute(sql, (url, job_id, data, daruma_code, url,))

        # commit the changes
        conn.commit()
        print("%s item was updated.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_request_job(requests_count, job_id):
    sql = """
    UPDATE job SET requests=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (requests_count, job_id))

        # commit the changes
        conn.commit()
        print("requests %s job was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def update_request_200_job(requests_count, job_id):
    sql = """
    UPDATE job SET requests_200=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (requests_count, job_id))

        # commit the changes
        conn.commit()
        print("requests %s job was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_image_url_by_item_url(item_url):

    sql = """
    SELECT image_url FROM price WHERE item_url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_url,))

        # commit the changes
        image_url = cur.fetchone()

        # close the cursor
        cur.close()
        return image_url[0]
    # except Exception as e:
    #     print("get_image_url_by_item_url: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def check_item(item_url):
    sql = """
    SELECT 1 FROM item WHERE url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_url,))
        result = cur.fetchone()
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        if result:
            return True
        else:
            return False
    except Exception as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def update_request(url, status_code, content_type, job_id):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE request SET http_status_code=%s, content_type=%s WHERE url=%s AND job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (status_code, content_type, url, job_id,))

        # commit the changes
        conn.commit()
        print("%s was updated.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def update_request_for_exception(url, exception_content):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE request SET exception_content=%s WHERE url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (exception_content, url,))

        # commit the changes
        conn.commit()
        print("error content for request with %s was updated.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def update_status_job(job_id, status):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE job SET status=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (status, job_id,))

        # commit the changes
        conn.commit()
        print("job %s status was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_status_job_with_process_id(job_id, status, process_id):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE job SET status=%s, process_id=%s, unit_id=%s WHERE job_id=%s
    """
    unit_id = process_id
    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (status, process_id, unit_id, job_id,))

        # commit the changes
        conn.commit()
        print("job %s status was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_link_id_price(item_url, link_id):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE price SET link_id=%s WHERE item_url=%s RETURNING price;
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (link_id, item_url,))

        price = cur.fetchone()
        price = price[0] if price is not None else None

        # commit the changes
        conn.commit()
        print("item_url %s link_id was updated.." % (item_url))

        # close the cursor
        cur.close()
        if price:
            return price
        else:
            return 0
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_price_id_and_item_name():
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    SELECT id, name, item_url FROM price WHERE brand=''
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql)
        result = []
        rows = cur.fetchall()
        for row in rows:
            result.append(
                dict(id=row[0],
                     name=row[1],
                     item_url=row[2])
            )

        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_price_id_by_url(item_url):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    SELECT id FROM price WHERE item_url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_url,))

        price_id = cur.fetchone()[0]

        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        return price_id
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def get_link_id_by_item_url(item_url):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    SELECT link_id FROM price WHERE item_url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_url,))
        link_id = cur.fetchone()
        # commit the changes
        conn.commit()
        return link_id[0]
        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def insert_user(username, password):
    """ insert vendor into users table """

    sql = """
    INSERT INTO users(username, password) VALUES(%s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (username, password,))

        # commit the changes
        conn.commit()
        print("%s with password %s was inserted into task table.." % (username, password))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        return "error"
    finally:
        # close conn
        if conn is not None:
            conn.close()


def get_tasks():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM task
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            task_id = row[0]
            task_name = row[1]
            index_url = row[2]
            product_url = row[3]
            brand_filter = row[4]
            detail_filter = row[5]
            task_type = row[6]
            status = row[7]
            requested_by = row[8]
            d = dict(task_id=task_id,
                     task_name=task_name,
                     index_url=index_url,
                     product_url=product_url,
                     brand_filter=brand_filter,
                     detail_filter=detail_filter,
                     task_type=task_type,
                     status=status,
                     requested_by=requested_by)
            result.append(d)

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job ORDER BY job_id DESC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7] if row[7] is not None else 0
            status = row[8]
            spider = row[9] if row[9] else "-"
            url_display = '%s..' % row[1][:50] if len(url) > 50 else url
            requests = row[11]
            requests_200 = row[12]
            priority = row[10]
            unit_id = row[14] if row[14] is not None else '-'
            speed = row[15] if row[15] is not None else 0
            repeat_days = row[16] if row[17] is not None else '-'
            schedule_time = row[17] if row[16] is not None else '-'
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               url_display=url_display,
                               requests=requests,
                               requests_200=requests_200,
                               spider=spider,
                               priority=priority,
                               unit_id=unit_id,
                               speed=speed,
                               schedule_time=schedule_time,
                               repeat_days=repeat_days))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_latest_job():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job ORDER BY job_id DESC LIMIT 1
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7] if row[7] is not None else 0
            status = row[8]
            spider = row[9] if row[9] else "-"
            url_display = '%s..' % row[1][:50] if len(url) > 50 else url
            requests = row[11]
            requests_200 = row[12]
            priority = row[10]
            unit_id = row[14] if row[14] is not None else '-'
            speed = row[15] if row[15] is not None else 0
            repeat_days = row[16] if row[17] is not None else '-'
            schedule_time = row[17] if row[16] is not None else '-'
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               url_display=url_display,
                               requests=requests,
                               requests_200=requests_200,
                               spider=spider,
                               priority=priority,
                               unit_id=unit_id,
                               speed=speed,
                               schedule_time=schedule_time,
                               repeat_days=repeat_days))

        # close the cursor
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("error get latest job: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_reviews_by_url(item_url):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM reviews WHERE item_url=%s ORDER BY created_at DESC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        rows = cur.fetchall()

        for row in rows:
            item_id = row[0]
            item_url = row[1]
            reviewer = row[2]
            review = row[3]
            rating = row[4]
            created_at = row[5]
            result.append(dict(item_id=item_id,
                               item_url=item_url,
                               reviewer=reviewer,
                               review=review,
                               rating=rating,
                               created_at=created_at,
                               ))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_auto_brand_name():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM auto_brand_name_table ORDER BY id DESC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            result.append(dict(id=row[0],
                               regex=row[1],
                               brand_name=row[2],
                               status=row[3]))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

# get product query
def get_product():
    """ query data from the product table """
    conn = None
    result = list()
    query = """
    SELECT * FROM product ORDER BY product_id DESC
    """

    def get_average(lst):
        return sum(lst) / len(lst)

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            product_id = row[7]
            link_id = row[0]
            name = row[1]
            brand = row[2]
            sku = row[3]
            supplier_code = row[4]
            description = row[5]
            average = round(row[8], 2) if row[8] is not None else 0
            lowest_price = row[9]
            highest_price = row[10]
            total_views = row[11]
            total_sales = row[12]
            result.append(dict(product_id=product_id,
                               link_id=link_id,
                               name=name,
                               brand=brand,
                               sku=sku,
                               supplier_code=supplier_code,
                               description=description,
                               average=average,
                               lowest_price=lowest_price,
                               highest_price=highest_price,
                               total_views=total_views,
                               total_sales=total_sales,
                               ))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

# get product query
def get_product_by_product_id(product_id):
    """ query data from the product table """
    conn = None
    result = list()
    query = """
    SELECT * FROM product WHERE product_id=%s
    """

    def get_average(lst):
        return sum(lst) / len(lst)

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (product_id,))

        rows = cur.fetchall()

        for row in rows:
            product_id = row[7]
            link_id = row[0]
            prod = get_price_column__by_link_id(link_id=link_id)
            price = [float(i.get('price')) for i in prod]
            average = round(get_average(price), 2)
            lowest_price = min(price)
            lowest_price_position = price.index(lowest_price)
            lowest_price_item_url = prod[lowest_price_position].get('item_url')
            highest_price = max(price)
            highest_price_position = price.index(highest_price)
            highest_price_item_url = prod[highest_price_position].get('item_url')
            total_views = sum([i for i in [i.get('views') for i in prod] if i is not None])
            total_sales = sum([i for i in [i.get('total_sales') for i in prod] if i is not None])
            name = row[1]
            brand = row[2]
            sku = row[3]
            supplier_code = row[4]
            description = row[5]
            result.append(dict(product_id=product_id,
                               link_id=link_id,
                               name=name,
                               brand=brand,
                               sku=sku,
                               supplier_code=supplier_code,
                               description=description,
                               average=average,
                               lowest_price=lowest_price,
                               highest_price=highest_price,
                               total_views=total_views,
                               total_sales=total_sales,
                               lowest_price_item_url=lowest_price_item_url,
                               highest_price_item_url=highest_price_item_url))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

# edit product table
def edit_aggregate(product_id, name, description, brand, sku, supplier_code):
    """ query data from the product table """
    conn = None
    result = list()
    query = """
    UPDATE product SET name=%s, description=%s, brand=%s, sku=%s, supplier_code=%s WHERE product_id=%s
    """

    def get_average(lst):
        return sum(lst) / len(lst)

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (name, description, brand, sku, supplier_code, product_id))

        # close the cursor
        conn.commit()
        cur.close()
        print("PRODUCT_TABLE: product_id %s was updated" % product_id)
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def is_status_on_process():
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT 1 FROM job WHERE status = 'on process' LIMIT 1;
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        result = cur.fetchone()

        # close the cursor
        cur.close()
        if result == None:
            return False
        elif result and len(result) > 0:
            return True
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def is_status_on_process_by_process_id(process_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT status FROM job WHERE process_id=%s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (process_id,))

        result = cur.fetchone()
        result = result[0] if result is not None else None

        # close the cursor
        cur.close()
        if result == 'on process':
            return True
        elif result == None:
            return False
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_start_time_by_job_id(job_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT start_time FROM job WHERE job_id=%s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        result = cur.fetchone()
        result = result[0] if result is not None else None

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_job_id_by_process_id(process_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT job_id FROM job WHERE process_id=%s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (process_id,))

        result = cur.fetchone()
        result = result[0] if result is not None else None

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def count_item_by_job_id(job_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT COUNT(*) FROM item WHERE job_id=%s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        result = cur.fetchone()
        result = result[0] if result is not None else None

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def is_priority_true():
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT 1 FROM job WHERE priority = TRUE AND status = 'pending' LIMIT 1;
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        result = cur.fetchone()

        # close the cursor
        cur.close()
        if result == None:
            return False
        elif result and len(result) > 0:
            return True
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_price_column__by_link_id(link_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT item_url, price, views, total_sales FROM price WHERE link_id=%s;
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (link_id,))

        items = cur.fetchall()
        result = list()
        for item in items:
            item_url = item[0]
            price = item[1]
            views = item[2]
            total_sales = item[3]
            result.append(dict(
                item_url=item_url,
                price=price,
                views=views,
                total_sales=total_sales
            ))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_job_by_id(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job WHERE job_id=%s ORDER BY job_id DESC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = len(get_requests_error(job_id=job_id))
            status = row[8]
            spider = row[9] if row[9] else "-"
            url_display = '%s..' % row[1][:50] if len(url) > 50 else url
            requests = len(get_requests(job_id))
            requests_200 = len(get_requests_200(job_id))
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               url_display=url_display,
                               requests=requests,
                               requests_200=requests_200,
                               spider=spider))

        # close the cursor
        cur.close()
        return result[0]
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_requests(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM request WHERE job_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        rows = cur.fetchall()

        count = 1

        for row in rows:
            job_id = row[0]
            url = row[1]
            http_status_code = row[2]
            content_type = row[3]
            content_length = row[4]
            created_at = row[5].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            exception_content = row[6].split("~") if row[6] is not None else "-"
            result.append(dict(
                count=count,
                job_id=job_id,
                url=url,
                http_status_code=http_status_code,
                content_type=content_type,
                content_length=content_length,
                created_at=created_at,
                exception_content=exception_content
            ))
            count += 1

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def count_request(job_id):
    """ query data from the vendors table """
    conn = None

    query = """
    select count(*) from request where job_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        result = cur.fetchone()


        # close the cursor
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def count_request_200(job_id):
    """ query data from the vendors table """
    conn = None

    query = """
        select count(*) from request where http_status_code='200' and job_id = %s
        """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        result = cur.fetchone()

        # close the cursor
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_requests_by_url(url, job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM request WHERE url=%s AND job_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (url, job_id,))

        rows = cur.fetchall()

        count = 1

        for row in rows:
            job_id = row[0]
            url = row[1]
            http_status_code = row[2]
            content_type = row[3]
            content_length = row[4]
            created_at = row[5].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            exception_content = row[6].split("~") if row[6] is not None else "-"
            result.append(dict(
                count=count,
                job_id=job_id,
                url=url,
                http_status_code=http_status_code,
                content_type=content_type,
                content_length=content_length,
                created_at=created_at,
                exception_content=exception_content
            ))
            count += 1

        # close the cursor
        cur.close()
        if result:
            return result[0]
        else:
            return None
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_requests_error(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM request WHERE job_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        rows = cur.fetchall()

        count = 1

        for row in rows:
            if row[6]:
                job_id = row[0]
                url = row[1]
                http_status_code = row[2]
                content_type = row[3]
                content_length = row[4]
                created_at = row[5].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
                error_name = row[6].split('~')[0] if row[6] is not None else "-"
                exception_content = row[6].split('~')[1] if row[6] is not None else "-"
                result.append(dict(
                    count=count,
                    job_id=job_id,
                    url=url,
                    http_status_code=http_status_code,
                    content_type=content_type,
                    content_length=content_length,
                    created_at=created_at,
                    error_name=error_name,
                    exception_content=exception_content
                ))
                count += 1
            else:
                pass

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_requests_200(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM request WHERE http_status_code='200' AND job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        rows = cur.fetchall()

        count = 1

        for row in rows:
            job_id = row[0]
            url = row[1]
            http_status_code = row[2]
            content_type = row[3]
            content_length = row[4]
            created_at = row[5].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            exception_content = row[6].split("~") if row[6] is not None else "-"
            result.append(dict(
                count=count,
                job_id=job_id,
                url=url,
                http_status_code=http_status_code,
                content_type=content_type,
                content_length=content_length,
                created_at=created_at,
                exception_content=exception_content
            ))
            count += 1

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_item():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM item
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            item_id = row[0]
            job_id = row[1]
            spider = row[7] if row[7] else "-"
            url = row[2]
            daruma_code = row[3]
            data = row[4]
            url_display = '%s..' % row[2][:100]
            result.append(dict(
                item_id=item_id,
                job_id=job_id,
                spider=spider,
                url=url,
                data=data,
                daruma_code=daruma_code,
                url_display=url_display
                # created_at=created_at
            ))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_item_byjobid(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM item WHERE job_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        rows = cur.fetchall()

        for row in rows:
            item_id = row[0]
            job_id = row[1]
            spider = row[7] if row[7] else "-"
            url = row[2]
            daruma_code = row[3]
            data = row[4]
            url_display = '%s..' % row[2][:100]
            result.append(dict(
                item_id=item_id,
                job_id=job_id,
                spider=spider,
                url=url,
                data=data,
                daruma_code=daruma_code,
                url_display=url_display
                # created_at=created_at
            ))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_item_byurl(url_item, tokped_product_id):
    """ query data from the vendors table """
    conn = None
    result = list()

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        if tokped_product_id:
            query = """
            SELECT * FROM item WHERE url=%s OR tokped_product_id=%s
            """
            cur.execute(query, (url_item, tokped_product_id,))
        else:
            query = """
                    SELECT * FROM item WHERE url=%s
                    """
            cur.execute(query, (url_item,))

        rows = cur.fetchall()

        for row in rows:
            item_id = row[0]
            job_id = row[1]
            spider = row[7] if row[7] else "-"
            url = row[2]
            daruma_code = row[3]
            data = row[4]
            url_display = '%s..' % row[2][:100]
            created_at = row[5]
            status = row[6]
            result.append(dict(
                item_id=item_id,
                job_id=job_id,
                spider=spider,
                url=url,
                data=data,
                daruma_code=daruma_code,
                url_display=url_display,
                created_at=created_at,
                status=status
            ))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_itemdata_byurl(url_item):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM item WHERE url=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (url_item,))

        rows = cur.fetchall()

        for i in range(len(rows)):
            data = rows[i][4]
            data = ast.literal_eval(data)
            data['created_at'] = get_item_byurl(url_item)[i]['created_at'] if len(
                get_item_byurl(url_item)) > 0 else None
            result.append(data)

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_job_byurl(url):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job WHERE url=%s ORDER BY created_at DESC
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (url,))

        rows = cur.fetchall()

        for row in rows:
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            result.append(dict(created_at=created_at))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_itemdata_byjobid(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM item WHERE job_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        rows = cur.fetchall()

        for row in rows:
            data = row[4]
            result.append(ast.literal_eval(data))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_item_brandlist():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM item WHERE spider='BrandListSpider'
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            item_id = row[0]
            job_id = row[1]
            spider = row[7] if row[7] else "-"
            url = row[2]
            daruma_code = row[3]
            data = row[4]
            url_display = '%s..' % row[2][:100]
            # created_at = row[3].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            result.append(dict(
                item_id=item_id,
                job_id=job_id,
                spider=spider,
                url=url,
                data=data,
                daruma_code=daruma_code,
                url_display=url_display
                # created_at=created_at
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_item_productlist():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM item WHERE spider='ProductListSpider'
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            item_id = row[0]
            job_id = row[1]
            spider = row[7] if row[7] else "-"
            url = row[2]
            daruma_code = row[3]
            data = row[4]
            url_display = '%s..' % row[2][:100]
            # created_at = row[3].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            result.append(dict(
                item_id=item_id,
                job_id=job_id,
                spider=spider,
                url=url,
                data=data,
                daruma_code=daruma_code,
                url_display=url_display
                # created_at=created_at
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_detail(id):
    """get detail of data by id"""
    conn = None
    query = """
    SELECT * FROM job WHERE job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (id,))

        details = cur.fetchall()

        # job_id
        # url
        # daruma_code
        # created_at
        # created_by
        # start_time
        # end_time
        # error_count
        dic = dict()
        for d in details:
            job_id = d[0]
            url = d[1]
            start_time = d[5]
            created_at = d[3].strftime('%m/%d/%Y %H:%M') if d[3] is not None else "-"
            created_by = d[4]
            end_time = d[6]
            error_count = len(get_requests_error(job_id=job_id))
            response_duration = "-"
            daruma_code = d[2]
            dic = dict(
                job_id=job_id,
                url=url,
                created_at=created_at,
                created_by=created_by,
                start_time=start_time if start_time is not None else "-",
                end_time=end_time if end_time is not None else "-",
                error_count=error_count,
                response_duration=response_duration,
                daruma_code=daruma_code
            )

        cur.close()
        return dic
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        if conn is not None:
            conn.close()


def delete_task(id):
    conn = None
    query = """
    DELETE FROM task WHERE task_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (id,))

        conn.commit()
        print("id %s was deleted" % id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()


def check_link_id(link_id):
    conn = None
    query = """
    select exists(select 1 from price where link_id=%s)
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (link_id,))
        result = cur.fetchone()
        conn.commit()
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def check_count_link_id(link_id):
    conn = None
    query = """
    select count(*) from price where link_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (link_id,))
        result = cur.fetchone()
        conn.commit()
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def delete_rows_by_link_id(link_id):
    conn = None
    query = """
    delete from product where link_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (link_id,))
        conn.commit()
        cur.close()
        print("link_id %s row was deleted from price table" % link_id)
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def get_price_by_item_url(item_url):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT * FROM price WHERE item_url=%s 
    """
    d = None
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (str(item_url),))

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2] if len(row[2]) is not 0 else "-"
            price_before_discount = row[3]
            price = row[4]
            sku = row[5] if len(row[5]) is not 0 else "-"
            supplier_code = row[6] if len(row[6]) is not 0 else "-"
            description = row[7] if row[7] is not None else "-"
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            store_url = get_store(item_url=item_url).get('store_url', "-") if get_store(
                item_url=item_url) is not None else "-"
            store_name = get_store(item_url=item_url).get('name', "-") if get_store(
                item_url=item_url) is not None else "-"
            rating = row[12] if row[12] is not 0 else "-"
            d = dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name,
                rating=rating
            )

        # close the cursor
        cur.close()
        return d
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price_by_id: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_price_by_link_id(link_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT price FROM price WHERE link_id=%s 
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (str(link_id),))

        rows = cur.fetchall()
        d = list()
        for price in rows:
            d.append(int(price[0]))

        # close the cursor
        cur.close()
        return d
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price_by_link_id: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def insert_product(link_id,
                   name,
                   brand,
                   sku,
                   supplier_code,
                   description,
                   last_update):


    sql = """
    INSERT INTO product(link_id, name, brand, sku, supplier_code, description, last_update) VALUES(%s, %s, %s, %s, %s, %s, %s) RETURNING product_id;
    """

    conn = None
    product_id = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (link_id,
                   name,
                   brand,
                   sku,
                   supplier_code,
                   description,
                   last_update))

        product_id = cur.fetchone()[0]
        # commit the changes
        conn.commit()
        print("INSERT PRODUCT: %s was inserted into product table.." % (name))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()
    return product_id

def get_price_data_in_product(link_id):


    sql = """
    SELECT price_data FROM product WHERE link_id=%s;
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (link_id,))

        price_data = cur.fetchone()
        price_data = price_data[0] if price_data is not None else None
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        if price_data:
            return ast.literal_eval(price_data)
        else:
            return price_data
    # except (Exception, pg.DatabaseError) as e:
    #     print("get_price_data_in_product error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def insert_price_data_product(link_id, price_list, price_id_list):


    sql = """
    UPDATE product SET price_data=%s WHERE link_id=%s;
    """

    price_data = get_price_data_in_product(link_id)
    if price_data:
        for p, id in zip(price_list, price_id_list):
            price_data[str(id)] = p
        price_data = str(price_data)
    else:
        price_data = dict()
        for p, id in zip(price_list, price_id_list):
            price_data[str(id)] = p
        price_data = str(price_data)
    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (price_data, link_id,))
        # price_data = price_data[0] if price_data is not None else None
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        print("insert_price_data_product: price data for link_id %s was updated" % link_id)
    except (Exception, pg.DatabaseError) as e:
        print("insert_price_data_product error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def insert_calculation_price_to_product(link_id, price_data):

    def get_average(lst):
        return sum(lst) / len(lst)

    price_list = [int(i.get('price')) for i in price_data]
    views = [int(i.get('views')) for i in price_data if i.get('views') is not None]
    total_sales = [int(i.get('total_sales')) for i in price_data if i.get('total_sales') is not None]

    average = get_average(price_list)
    min_price = min(price_list)
    max_price = max(price_list)
    total_views = sum(views)
    total_sales = sum(total_sales)

    sql = """
    UPDATE product SET price_average=%s, min_price=%s, max_price=%s, total_views=%s, total_sales=%s WHERE link_id=%s;
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (average, min_price, max_price, total_views, total_sales, link_id))
        # price_data = price_data[0] if price_data is not None else None
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        print("insert_calculation_price_to_product: price data for link_id %s was updated" % link_id)
    except (Exception, pg.DatabaseError) as e:
        print("insert_calculation_price_to_product error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_price_from_price_table(link_id):


    sql = """
    SELECT price, views, total_sales FROM price WHERE link_id=%s
    """

    conn = None
    result = list()
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (link_id,))
        price = cur.fetchall()

        for p in price:
            price = p[0]
            views = p[1]
            total_sales = p[2]
            result.append(dict(
                price=price,
                views=views,
                total_sales=total_sales
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("insert_price_data_product error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def remove_price_data_product(link_id, price_id_list):


    sql = """
    UPDATE product SET price_data=%s WHERE link_id=%s;
    """

    price_data = get_price_data_in_product(link_id)
    if price_data:
        for u in price_id_list:
            del price_data[str(u)]
        price_data = str(price_data)
    else:
        price_data = dict()
        for u in price_id_list:
            del price_data[str(u)]
        price_data = str(price_data)

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (price_data, link_id,))
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        print("remove_price_data_product: price data for link_id %s was updated" % link_id)
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def get_price(store=None):
    """ query data from the vendors table """
    conn = None
    result = list()

    if store:
        query = """
                SELECT * FROM price WHERE store=%s ORDER BY id DESC
                """
    else:
        query = """
                SELECT * FROM price ORDER BY id DESC
                """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        if store:
            cur.execute(query, (store,))
        else:
            cur.execute(query)


        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount = row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            store_url = row[12] if row[12] else row[9]
            store_name = row[9],
            link_id = row[15] if row[15] is not None else '-'
            sales=row[16]
            views=row[17]
            reviews=row[18] if row[18] is not None else 0
            total_sales=row[19]
            image_url=row[14]
            stock = row[20] if row[20] is not None else '-'
            mpn = row[24] if row[24] is not None else '-'
            result.append(dict(
                price_id=price_id,
                mpn=mpn,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name,
                link_id=link_id,
                sales=sales,
                views=views,
                reviews=reviews,
                total_sales=total_sales,
                image_url=image_url,
                stock=stock
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_store_name_from_price_table():
    """ query data from the vendors table """
    conn = None
    result = list()

    query = """
        SELECT store FROM price;
        """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query)


        rows = cur.fetchall()

        for row in rows:
            store_name = row[0]
            result.append(store_name)

        # close the cursor
        cur.close()
        return list(set(result))
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_price_for_for_download():
    """ query data from the vendors table """
    conn = None
    result = list()

    query = """
        SELECT * FROM price ORDER BY id DESC
        """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query)


        rows = cur.fetchall()

        for row in rows:
            id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount = row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            store_name = row[9],
            link_id = row[15] if row[15] is not None else '-'
            sales=row[16]
            views=row[17]
            reviews=row[18] if row[18] is not None else 0
            total_sales=row[19]
            image_url=row[14]
            stock = row[20] if row[20] is not None else '-'
            mpn = row[24] if row[24] is not None else '-'
            result.append(dict(
                id=id,
                mpn=mpn,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_name=store_name,
                link_id=link_id,
                sales=sales,
                views=views,
                reviews=reviews,
                total_sales=total_sales,
                image_url=image_url,
                stock=stock
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_price_order_by_total_sales(store=None):
    """ query data from the vendors table """
    conn = None
    result = list()
    if store:
        query = """
            SELECT * FROM price WHERE store=%s ORDER BY total_sales DESC
            """
    else:
        query = """
            SELECT * FROM price ORDER BY total_sales DESC
            """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        if store:
            cur.execute(query, (store,))
        else:
            cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount = row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            store_url = row[12] if row[12] else row[9]
            store_name = row[9],
            link_id = row[15] if row[15] is not None else '-'
            sales=row[16]
            views=row[17]
            reviews=row[18] if row[18] is not None else 0
            total_sales=row[19]
            image_url = row[14]
            result.append(dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name,
                link_id=link_id,
                sales=sales,
                views=views,
                reviews=reviews,
                total_sales=total_sales,
                image_url=image_url
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_price_order_by_views(store=None):
    """ query data from the vendors table """
    conn = None
    result = list()
    if store:
        query = """
            SELECT * FROM price WHERE store=%s ORDER BY views DESC
            """
    else:
        query = """
            SELECT * FROM price ORDER BY views DESC
            """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        if store:
            cur.execute(query, (store,))
        else:
            cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount = row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            store_url = row[12] if row[12] else row[9]
            store_name = row[9],
            link_id = row[15] if row[15] is not None else '-'
            sales=row[16]
            views=row[17]
            reviews=row[18] if row[18] is not None else 0
            total_sales=row[19]
            image_url = row[14]
            result.append(dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name,
                link_id=link_id,
                sales=sales,
                views=views,
                reviews=reviews,
                total_sales=total_sales,
                image_url=image_url
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_price_order_by_sales(store=None):
    """ query data from the vendors table """
    conn = None
    result = list()

    if store:
        query = """
            SELECT * FROM price WHERE store=%s ORDER BY sales DESC
            """
    else:
        query = """
            SELECT * FROM price ORDER BY sales DESC
            """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        if store:
            cur.execute(query, (store,))
        else:
            cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount = row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            store_url = row[12] if row[12] else row[9]
            store_name = row[9],
            link_id = row[15] if row[15] is not None else '-'
            sales=row[16]
            views=row[17]
            reviews=row[18] if row[18] is not None else 0
            total_sales=row[19]
            image_url = row[14]
            result.append(dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name,
                link_id=link_id,
                sales=sales,
                views=views,
                reviews=reviews,
                total_sales=total_sales,
                image_url=image_url
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def download_job_as_csv():
    """ query data from the vendors table """
    try:
        os.remove('job_table.csv')
    except:
        pass
    finally:
        conn = None
        query = """
        SELECT * FROM job ORDER BY id DESC
        """

        try:
            conn = pg.connect(**params)
            cur = conn.cursor()

            outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)

            with open('price_table.csv', 'w', encoding='utf-8') as f:
                cur.copy_expert(outputquery, f)
                f.close()
            print("job table was downloaded")
            # close the cursor
            cur.close()
        except (Exception, pg.DatabaseError) as e:
            print("oops in get_price: ", e)
        finally:
            # close the conn
            if conn is not None:
                conn.close()

def download_price_as_csv(sorter, store_sorter):
    """ query data from the vendors table """
    try:
        os.remove('price_table.csv')
    except:
        pass
    finally:
        price = None
        if sorter or store_sorter:
            if sorter == 'total sales':
                price = get_price_order_by_total_sales(store_sorter)
            elif sorter == 'views':
                price = get_price_order_by_views(store_sorter)
            elif sorter == 'sales':
                price = get_price_order_by_sales(store_sorter)
            else:
                if store_sorter == 'no_filter' or store_sorter == '':
                    price = get_price_for_for_download()
                else:
                    price = get_price(store_sorter)
        keys = price[0].keys()
        with open('price_table.csv', 'w', encoding='utf-8') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(price)

def download_jobs():
    """ query data from the vendors table """
    try:
        os.remove('jobs.csv')
    except:
        pass
    finally:
        conn = None
        query = """
        SELECT * FROM job ORDER BY job_id DESC
        """

        try:
            conn = pg.connect(**params)
            cur = conn.cursor()

            outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)

            with open('job_table.csv', 'w', encoding='utf-8') as f:
                cur.copy_expert(outputquery, f)
                f.close()
            print("job table was downloaded")
            # close the cursor
            cur.close()
        except (Exception, pg.DatabaseError) as e:
            print("oops in get_price: ", e)
        finally:
            # close the conn
            if conn is not None:
                conn.close()

def download_items():
    """ query data from the vendors table """
    try:
        os.remove('item_table.csv')
    except:
        pass
    finally:
        conn = None
        query = """
        SELECT * FROM item ORDER BY job_id DESC
        """

        try:
            conn = pg.connect(**params)
            cur = conn.cursor()

            outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)

            with open('item_table.csv', 'w', encoding='utf-8') as f:
                cur.copy_expert(outputquery, f)
                f.close()
            print("item table was downloaded")
            # close the cursor
            cur.close()
        except (Exception, pg.DatabaseError) as e:
            print("oops in get_price: ", e)
        finally:
            # close the conn
            if conn is not None:
                conn.close()


def download_request():
    """ query data from the vendors table """
    try:
        os.remove('request_table.csv')
    except:
        pass
    finally:
        conn = None
        query = """
        SELECT * FROM request ORDER BY job_id DESC
        """

        try:
            conn = pg.connect(**params)
            cur = conn.cursor()

            outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)

            with open('request_table.csv', 'w', encoding='utf-8') as f:
                cur.copy_expert(outputquery, f)
                f.close()
            print("job table was downloaded")
            # close the cursor
            cur.close()
        except (Exception, pg.DatabaseError) as e:
            print("oops in get_price: ", e)
        finally:
            # close the conn
            if conn is not None:
                conn.close()


def download_item():
    """ query data from the vendors table """
    try:
        os.remove('item_table.csv')
    except:
        pass
    finally:
        conn = None
        query = """
        SELECT * FROM item ORDER BY item_id DESC
        """

        try:
            conn = pg.connect(**params)
            cur = conn.cursor()

            outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)

            with open('item_table.csv', 'w', encoding='utf-8') as f:
                cur.copy_expert(outputquery, f)
                f.close()
            print("item table was downloaded")
            # close the cursor
            cur.close()
        except (Exception, pg.DatabaseError) as e:
            print("oops in get_price: ", e)
        finally:
            # close the conn
            if conn is not None:
                conn.close()


def search_price(keyword):
    """ query data from the vendors table """
    conn = None
    result = list()
    # query = f"""
    # SELECT * FROM price WHERE name ILIKE '%{keyword}%'
    # OR daruma_code ILIKE '%{keyword}%'
    # OR description ILIKE '%{keyword}%'
    # OR brand ILIKE '%{keyword}%'
    # OR store ILIKE '%{keyword}%'
    # ORDER BY id DESC
    # """

    query = f"""
        SELECT * FROM price WHERE name ILIKE '%{keyword}%' 
        OR daruma_code ILIKE '%{keyword}%' 
        AND daruma_code ILIKE '%{keyword}%' 
        OR description ILIKE '%{keyword}%'
        AND description ILIKE '%{keyword}%'
        OR brand ILIKE '%{keyword}%'
        AND brand ILIKE '%{keyword}%'
        OR store ILIKE '%{keyword}%'
        AND store ILIKE '%{keyword}%'
        OR link_id ILIKE '%{keyword}%'
        AND link_id ILIKE '%{keyword}%'
        ORDER BY id DESC
        """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount = row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            link_id = row[15] if row[15] is not None else '-'
            store_url = row[13]
            store_name = get_store(item_url=item_url).get('name', None) if get_store(
                item_url=item_url) is not None else "-",
            sales =row[16] if row[16] is not None else 0
            views = row[17] if row[17] is not None else 0
            reviews = row[18] if row[18] is not None else 0
            total_sales = row[19] if row[19] is not None else 0
            result.append(dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name,
                link_id=link_id,
                sales=sales,
                views=views,
                reviews=reviews,
                total_sales=total_sales
            ))

        # close the cursor
        cur.close()
        return result
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_store(item_url):
    conn = None
    query = """
    SELECT * FROM store WHERE item_url=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        rows = cur.fetchall()

        for row in rows:
            item_url = row[0]
            name = row[1]
            store_url = row[2]
            type = row[3]
            return dict(
                item_url=item_url,
                name=name,
                store_url=store_url,
                type=type,
            )

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_store: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_price_by_id(price_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT * FROM price WHERE id=%s 
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (str(price_id),))

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2] if len(row[2]) is not 0 else "-"
            price_before_discount = row[3]
            price = row[4]
            sku = row[5] if len(row[5]) is not 0 else "-"
            supplier_code = row[6] if len(row[6]) is not 0 else "-"
            desc = row[7] if row[7] is not None else "-"
            description = "%s.." % desc[:100] if len(desc) > 40 else desc
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            # store_url = get_store(item_url=item_url).get('store_url', "-") if get_store(
            #     item_url=item_url) is not None else "-"
            # store_name = get_store(item_url=item_url).get('name', "-") if get_store(
            #     item_url=item_url) is not None else "-"
            rating = row[12] if row[12] is not 0 else "-"
            image_url = row[14]
            store_url = row[12] if row[12] else row[9]
            store_name = row[9],
            link_id = row[15] if row[15] is not None else '-'
            sales = row[16]
            views = row[17]
            reviews = row[18]
            total_sales = int(sales) * int(price)
            d = dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name,
                rating=rating,
                image_url=image_url,
                link_id=link_id,
                views=views,
                reviews=reviews,
                total_sales=total_sales,
                sales=sales,
                full_description=desc
            )

        # close the cursor
        cur.close()
        return d
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price_by_id: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def ge_price_by_link_id(link_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT * FROM price WHERE id=%s 
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (str(link_id),))

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2] if len(row[2]) is not 0 else "-"
            price_before_discount = row[3]
            price = row[4]
            sku = row[5] if len(row[5]) is not 0 else "-"
            supplier_code = row[6] if len(row[6]) is not 0 else "-"
            description = row[7] if row[7] is not None else "-"
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            store_url = get_store(item_url=item_url).get('store_url', "-") if get_store(
                item_url=item_url) is not None else "-"
            store_name = get_store(item_url=item_url).get('name', "-") if get_store(
                item_url=item_url) is not None else "-"
            rating = row[12] if row[12] is not 0 else "-"
            d = dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name,
                rating=rating
            )

        # close the cursor
        cur.close()
        return d
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price_by_id: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def delete_job(id):
    conn = None
    query = """
    DELETE FROM job WHERE job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (id,))

        conn.commit()
        print("id %s was deleted" % id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("DELETE JOB FAILED: ", e)
        send_report(note="ERROR IN DELETE JOB METHOD", exception_content=e)
    finally:
        if conn is not None:
            conn.close()


def delete_job_by_status(status):
    conn = None
    query = """
    DELETE FROM job WHERE status = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (status,))

        conn.commit()
        print("id %s was deleted" % status)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def get_brand_from_data_auto(item_url):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT data_auto FROM price WHERE item_url=%s
    """


    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        result = cur.fetchone()
        result = result[0] if result is not None else None
        if result:
            result = ast.literal_eval(result)
            result = result.get('brand', None)
            if result:
                if len(result) < 2:
                    result = None

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def delete_user(user_id):
    conn = None
    query = """
    DELETE FROM users WHERE user_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (user_id,))

        conn.commit()
        print("User with id %s was deleted" % user_id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()


def get_users():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT user_id, username, password FROM users
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            user_id = row[0]
            username = row[1]
            password = row[2]
            d = dict(user_id=user_id, username=username, password=password)
            result.append(d)

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def check_username(username, password):
    """ query data from the vendors table """
    conn = None
    result = None
    query = """
    SELECT password FROM users WHERE username = %s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (username,))

        result = cur.fetchone()[0]

        # close the cursor
        cur.close()
        if result == password:
            return "success"
        else:
            return "error"
    except (Exception, pg.DatabaseError) as e:
        return "error"
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def dropTable(tableName):
    conn = None
    query = "DROP TABLE IF EXISTS {} CASCADE;".format(tableName)
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        cur.close()
        conn.commit()
        print("%s table was deleted" % tableName)

    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()


def get_pending_productlist_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job WHERE status='pending' AND spider='ProductListSpider'
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7]
            status = row[8]
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_pending_brandlist_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job WHERE status='pending' AND spider='BrandListSpider'
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7]
            status = row[8]
            spider = row[9] if row[9] else "-"
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               spider=spider))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_pending_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job WHERE status='pending' and schedule_time is null ORDER BY job_id ASC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7]
            status = row[8]
            spider = row[9] if row[9] else "-"
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               spider=spider))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_scheduled_pending_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job WHERE status='pending' AND schedule_time IS NOT NULL ORDER BY schedule_time ASC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7]
            status = row[8]
            spider = row[9] if row[9] else "-"
            priority = row[10]
            schedule_time = row[17]
            repeat_days = row[16]
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               spider=spider,
                               schedule_time=schedule_time,
                               repeat_days=repeat_days,
                               priority=priority))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_repeat_days(job_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT repeat_days FROM job WHERE job_id=%s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        result = cur.fetchone()[0]

        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_spider(job_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT spider FROM job WHERE job_id=%s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        result = cur.fetchone()[0]

        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_priority_pending_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job WHERE status='pending' AND priority = TRUE ORDER BY job_id ASC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7]
            status = row[8]
            spider = row[9] if row[9] else "-"
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               spider=spider))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def restart():
    dropTable("job")
    create_tables()


def update_endtime_job(job_id, end_time):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE job SET end_time=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (end_time, job_id,))

        # commit the changes
        conn.commit()
        print("end_time job %s was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def delete_dropped_item(item_id):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    DELETE FROM dropped_item WHERE item_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_id))

        # commit the changes
        conn.commit()
        print("item id %s was deleted.." % (item_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()



def get_error_count(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT error_count FROM job WHERE job_id=%s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        row = cur.fetchone()
        row = 0 if row[0] == None else row
        # close the cursor
        cur.close()
        return row
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


# def insert_error_count(job_id):
#     """ insert vendor into vendors table """
#
#     error = int(get_error_count(job_id=job_id)) + 1
#
#     sql = """
#     UPDATE job SET error_count=%s WHERE job_id=%s
#     """
#
#     # UPDATE job SET start_time=%s WHERE job_id=%s
#
#     conn = None
#     try:
#         conn = pg.connect(**params)
#
#         # create new cursor
#         cur = conn.cursor()
#         # execute sql query
#         cur.execute(sql, (error, job_id,))
#
#         # commit the changes
#         conn.commit()
#         print("%s error_count was updated into job table.." % (job_id))
#
#         # close the cursor
#         cur.close()
#     except (Exception, pg.DatabaseError) as e:
#         print("oops: ", e)
#     finally:
#         # close conn
#         if conn is not None:
#             conn.close()


def update_starttime_job(job_id, start_time):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE job SET start_time=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (start_time, job_id,))

        # commit the changes
        conn.commit()
        print("start_time job %s was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_data_auto(url_item):

    sql = """
       select data_auto from price where item_url=%s
       """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (url_item,))

        result = cur.fetchone()
        result = result[0] if result is not None else None
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        if result:
            result = ast.literal_eval(result)
            return result

        return result
    except (Exception, pg.DatabaseError) as e:
        print("update_data_auto error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_brand_from_data_original(item_url):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT data_original FROM price WHERE item_url=%s
    """


    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        result = cur.fetchone()
        result = result[0] if result is not None else None
        if result:
            result = ast.literal_eval(result)
            result = result.get('brand', None)
            if result:
                if len(result) < 2:
                    result = None

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_name_from_data_original(item_url):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT data_original FROM price WHERE item_url=%s
    """


    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        result = cur.fetchone()
        result = result[0] if result is not None else None
        if result:
            result = ast.literal_eval(result)
            result = result.get('name', None)
            if result:
                if len(result) < 2:
                    result = None

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def read_price_csv(filename):
    reader = csv.DictReader(open(filename, encoding='utf-8'))
    result = list()
    for row in reader:

        brand_auto = get_brand_from_data_auto(item_url=row['item_url'])
        if row['brand'] is not None or len(str(row['brand']).strip()) > 1:
            brand = row['brand']
        elif brand_auto is not None:
            brand = brand_auto
        else:
            brand = get_brand_from_data_original(item_url=row['item_url'])

        name_original = get_name_from_data_original(item_url=row['item_url'])
        name = None
        if row['name'] is not None or len(str(row['name']).strip()) > 1:
            name = row['name']
        elif name_original is not None:
            name = name_original


        r = dict(
        id=int(row['id']),
        mpn = row['mpn'],
        name = name,
        brand = brand,
        price_before_discount = int(row['price_before_discount']),
        price = int(row['price']),
        sku = row['sku'],
        supplier_code = row['supplier_code'],
        description = row['description'],
        item_url = row['item_url'],
        store = row['store'],
        last_update = datetime.now(),
        daruma_code = row['daruma_code'],
        store_name = ast.literal_eval(row['store_name'])[0] if row['store_name'] is not None else '-',
        link_id = row['link_id'],
        sales = int(row['sales']),
        views = int(row['views']),
        reviews = int(row['reviews']),
        total_sales = int(row['total_sales']),
        image_url = row['image_url'],
        stock = int(row['stock'])
        )
        r['data_custom'] = str(r)
        result.append(r)
    return tuple(result)

def read_job_csv(filename):
    reader = csv.DictReader(open(filename, encoding='utf-8'))
    result = list()
    for row in reader:
        r = dict(
        job_id=int(row['job_id']),
        url = row['url'],
        daruma_code = row['daruma_code'],
        created_at = row['created_at'],
        created_by = row['created_by'],
        start_time = row['start_time'],
        end_time = row['end_time'],
        error_count = int(row['error_count']) if row['error_count'] is not '' else 0,
        status = row['status'],
        spider = row['spider'],
        priority = False if row['priority'] is False else True,
        requests = int(row['requests']),
        requests_200 = int(row['requests_200']),
        process_id = row['process_id'],
        unit_id = row['unit_id'],
        speed = float(row['speed']) if row['speed'] is not '' else 0,
        repeat_days = int(row['repeat_days']) if row['repeat_days'] is not '' else 0,
        schedule_time = row['schedule_time'] if row['schedule_time'] is not '' else None
        )
        result.append(r)
    return tuple(result)

def update_bulk_price(filename):

    data = read_price_csv(filename)

    sql = """
    UPDATE price 
    SET 
        brand = %(brand)s,
        daruma_code = %(daruma_code)s,
        description = %(description)s,
        image_url = %(image_url)s,
        item_url = %(item_url)s,
        last_update = %(last_update)s,
        link_id = %(link_id)s,
        mpn = %(mpn)s,
        name = %(name)s,
        price = %(price)s,
        price_before_discount = %(price_before_discount)s,
        id = %(id)s,
        reviews = %(reviews)s,
        sales = %(sales)s,
        sku = %(sku)s,
        stock = %(stock)s,
        store = %(store)s,
        supplier_code = %(supplier_code)s,
        total_sales = %(total_sales)s,
        views = %(total_sales)s,
        data_custom = %(data_custom)s
    WHERE
        item_url = %(item_url)s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.executemany(sql, data)

        # commit the changes
        conn.commit()
        print("price table was updated")

        # close the cursor
        cur.close()
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def insert_bulk_job(filename):

    data = read_job_csv(filename)

    sql = """
    INSERT INTO 
    job(
    job_id,
    url,
    daruma_code,
    created_at,
    created_by,
    start_time,
    end_time,
    error_count,
    status,
    spider,
    priority,
    requests,
    requests_200,
    process_id,
    unit_id,
    speed,
    repeat_days,
    schedule_time
    ) 
    VALUES(
    %(job_id)s, 
    %(url)s, 
    %(daruma_code)s, 
    %(created_at)s, 
    %(created_by)s, 
    %(start_time)s, 
    %(end_time)s,
    %(error_count)s,
    %(status)s,
    %(spider)s,
    %(priority)s,
    %(requests)s,
    %(requests_200)s,
    %(process_id)s,
    %(unit_id)s,
    %(speed)s,
    %(repeat_days)s,
    %(schedule_time)s
    )
    """

    # sql = """
    # UPDATE price
    # SET
    #     brand = %(brand)s,
    #     daruma_code = %(daruma_code)s,
    #     description = %(description)s,
    #     image_url = %(image_url)s,
    #     item_url = %(item_url)s,
    #     last_update = %(last_update)s,
    #     link_id = %(link_id)s,
    #     mpn = %(mpn)s,
    #     name = %(name)s,
    #     price = %(price)s,
    #     price_before_discount = %(price_before_discount)s,
    #     id = %(id)s,
    #     reviews = %(reviews)s,
    #     sales = %(sales)s,
    #     sku = %(sku)s,
    #     stock = %(stock)s,
    #     store = %(store)s,
    #     supplier_code = %(supplier_code)s,
    #     total_sales = %(total_sales)s,
    #     views = %(total_sales)s,
    #     data_custom = %(data_custom)s
    # WHERE
    #     item_url = %(item_url)s
    # """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.executemany(sql, data)

        # commit the changes
        conn.commit()
        print("job table was updated")

        # close the cursor
        cur.close()
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


if __name__ == "__main__":
    # name_and_id = get_price_id_and_item_name()
    # print(name_and_id)
    # scheduled_job = get_scheduled_pending_jobs()
    # if len(scheduled_job) > 0:
    #     job = scheduled_job[0]
    #     datetime_job = job['schedule_time']
    #     next_schedule_time = datetime_job + timedelta(days=1)
    #     print(datetime_job.strftime("%Y-%m-%d %H:%M"))
    #     print(next_schedule_time.strftime("%Y-%m-%d %H:%M"))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
    #     print(datetime.now().strftime("%Y-%m-%d %H:%M"))

    # for index in range(0, 9):
    #     delete_job(int(f'32{index}'))
    # delete_job(361)
    # insert_job(url='https://www.tokopedia.com/darumacoid', created_by='farhan',
    #            daruma_code=None, spider='ProductListSpider',priority=False,
    #            schedule_time=None)


    # print(check_username('farhan', 'daruma123'))
    # print(get_reviews_by_url('https://www.tokopedia.com/darumacoid/sanwa-mini-ball-valve-model-bv-15-mf-size-1-2'))
    # delete_job(43)
    # delete_job(40)
    print(get_store_name_from_price_table())
    # download_jobs()
    # print(read_job_csv('job_table.csv')[0])
    # insert_bulk_job('job_table.csv')
    # delete_job(147)
    # pprint.pprint(get_jobs()[0])
    # auto_insert_job(url='https://tokopedia.com/darumacoid', daruma_code='', priority=False, created_by='farhan', spider='ProductListSpider', schedule_time=datetime.now() + timedelta(minutes=3), repeat_days=1)
    # pprint.pprint(get_jobs()[0])
    # print(get_spider(19))
    # print(len(name_and_id))
    # data_auto = get_data_auto('https://www.tokopedia.com/wd-40specialist/wd40-machine-engine-degreaser-heavy-duty-brake-cleaner-carb-clean')
    # pprint.pprint(data_auto)
