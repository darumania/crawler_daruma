# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from postgresql import *
import time

class PricePipeline(object):
    def process_item(self, item, spider):
        time.sleep(0.5)
        insert_item(url=item['url'], job_id=item['job_id'], data=str(item), tokped_product_id=item['tokped_product_id'])
        return item

