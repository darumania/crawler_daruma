# -*- coding: utf-8 -*-
import scrapy
import csv
from urllib.parse import urlparse


class DarumaSpider(scrapy.Spider):
    name = 'daruma'
    # allowed_domains = ['https://www.daruma.co.id']
    urls = ['https://www.daruma.co.id/screwdriver-for-broken-screws-275-9411.html/']

    def start_requests(self):
        index_url = getattr(self, 'index_url', None)

        if index_url is not None:

            if index_url.startswith("http"):
                yield scrapy.Request(index_url, self.parse_index)
            else:
                data = []
                with open(index_url, "rt") as f:
                    for line in csv.reader(f.readlines(), quotechar='"', delimiter=',',
                        quoting=csv.QUOTE_ALL, skipinitialspace=True):
                        sku = line[0].strip()
                        url = line[1].strip()
                        data.append({'sku': sku, 'url': url})

                for r in data:
                    yield scrapy.Request(r['url'], self.parse_product, meta=r)
        else:
            for url in self.urls:
                yield scrapy.Request(url, self.parse_product)

    def parse_index(self, response):

        for line in csv.reader(response.text.splitlines(), quotechar='"', delimiter=',',
            quoting=csv.QUOTE_ALL, skipinitialspace=True):
            if len(line) != 2:
                continue
            sku = line[0].strip()
            url = line[1].strip()

            r = {'sku': sku, 'url': url}
            yield scrapy.Request(r['url'], self.parse_product, meta=r)

    def parse_product(self, response):

        domain = urlparse(response.url).netloc
        if domain.startswith("www."):
            domain = domain[4:]

        if domain.lower() == "daruma.co.id":
            return self.parse_daruma(response)
        elif domain.lower() == "monotaro.id":
            return self.parse_monotaro(response)
        elif domain.lower() == "kawanlama.com":
            return self.parse_kawanlama(response)
        elif domain.lower() == "tokopedia.com":
            return self.parse_tokopedia(response)
        elif domain.lower() == "bukalapak.com":
            return self.parse_bukalapak(response)
        elif domain.lower() == "lazada.co.id":
            return self.parse_lazada(response)
        elif domain.lower() == "blibli.com":
            return self.parse_blibli(response)
        else:
        	print("Invalid domain:", domain)

    def parse_daruma(self, response):

        sku = response.meta.get("sku")
        title = response.css(".page-title").xpath("span/text()").extract_first().strip()
        price = response.css(".product-info-price").css(".price-wrapper::attr(data-price-amount)").extract_first().strip()
        seller = "Daruma"

        yield {'url': response.url, 'sku': sku, 'title': title, 'price': price, 'seller': seller}

    def parse_monotaro(self, response):

        sku = response.meta.get("sku")
        title = response.css(".product-name").xpath("h1/text()").extract_first().strip()
        price = response.css(".price-box").css(".price::text").extract_first().strip()
        price = price.replace("Rp", "").replace(".", "")
        seller = "Monotaro"

        yield {'url': response.url, 'sku': sku, 'title': title, 'price': price, 'seller': seller}

    def parse_kawanlama(self, response):

        sku = response.meta.get("sku")
        title = response.css(".product-name .h1::text").extract_first().strip()
        price = response.css(".add-to-cart").css(".price::text").extract_first()
        price = price.replace("Rp", "").replace(".", "").replace(",", "")
        seller = "Kawanlama"

        yield {'url': response.url, 'sku': sku, 'title': title, 'price': price, 'seller': seller}

    def parse_tokopedia(self, response):

        sku = response.meta.get("sku")
        title = response.css(".product-title").xpath("a/text()").extract_first().strip()
        price = response.css(".product-box-content > .product-pricetag").xpath('span[@itemprop="price"]/text()').extract_first().strip()
        price = price.replace("Rp", "").replace(".", "")
        seller = response.css('#shop-name-info::text').extract_first().strip()

        yield {'url': response.url, 'sku': sku, 'title': title, 'price': price, 'seller': "Tokopedia/{}".format(seller)}

    def parse_bukalapak(self, response):

        sku = response.meta.get("sku")
        title = response.css(".c-product-detail__name::text").extract_first().strip()
        price = response.css(".c-product-detail-price::attr(data-reduced-price)").extract_first().strip()
        price = price.replace("Rp", "").replace(".", "")
        seller = response.css(".c-user-identification__name::text").extract_first().strip()

        yield {'url': response.url, 'sku': sku, 'title': title, 'price': price, 'seller': "Bukalapak/{}".format(seller)}

    def parse_lazada(self, response):

        sku = response.meta.get("sku")
        title = response.css("#prod_title::text").extract_first().strip()
        price = response.css("#special_price_box::text").extract_first().strip()
        price = price.replace("Rp", "").replace(".", "")
        seller = response.css(".seller-details").css(".basic-info__name::text").extract_first().strip()

        yield {'url': response.url, 'sku': sku, 'title': title, 'price': price, 'seller': "Lazada/{}".format(seller)}

    def parse_blibli(self, response):

        sku = response.meta.get("sku")
        title = response.css(".product-name::text").extract_first().strip()
        price = ""
        price = price.replace("Rp", "").replace(".", "")
        seller = ""

        yield {'url': response.url, 'sku': sku, 'title': title, 'price': price, 'seller': "Blibli/{}".format(seller)}
